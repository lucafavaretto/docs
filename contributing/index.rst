.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

Contributing to |main_project_name|
###################################

The contributing process and guidelines part of this document must be applied
to any repository in the scope of the |main_project_name| project. Each
repository must include this information in its ``CONTRIBUTING.md`` file and
optionally, complement the process and guidelines with repository-specific
requirements.

.. toctree::
   :maxdepth: 1
   
   quick-start-contribution-onboarding
   eca
   gitlab
   reuse
   dco
   upstream_contribution_process
   devtool
   bug_policy
